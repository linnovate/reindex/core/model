
ARG PLUGINS_IMAGE_REF=""

FROM $PLUGINS_IMAGE_REF AS plugins

FROM node:10.15

ARG CACHEBUST=1

RUN echo $CACHEBUST;

COPY . /usr/src/app

WORKDIR /usr/src/app

RUN npm install

COPY --from=plugins usr/src/app node_modules/@reindex

RUN cd node_modules/@reindex/plugins && npm install 

CMD [ "npm", "start" ]